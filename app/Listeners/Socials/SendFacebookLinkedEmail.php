<?php

namespace App\Listeners\Socials;

use Mail;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\Socials\FacebookAccountLinked;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Socials\FacebookAccountWasLinked;

class SendFacebookLinkedEmail
{
    public $user;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle the event.
     *
     * @param  FacebookAccountWasLinked  $event
     * @return void
     */
    public function handle(FacebookAccountWasLinked $event)
    {
        Mail::to($event->user)->send(new FacebookAccountLinked($event->user));
    }
}
