<?php

namespace App\Listeners\Socials;

use Mail;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\Socials\GithubAccountLinked;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Socials\GithubAccountWasLinked;

class SendGithubLinkedEmail
{
    public $user;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle the event.
     *
     * @param  GithubAccountWasLinked  $event
     * @return void
     */
    public function handle(GithubAccountWasLinked $event)
    {
        Mail::to($event->user)->send(new GithubAccountLinked($event->user));
    }
}
