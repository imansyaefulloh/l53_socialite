<?php

namespace App\Listeners\Socials;

use Mail;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\Socials\TwitterAccountLinked;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Socials\TwitterAccountWasLinked;

class SendTwitterLinkedEmail
{
    public $user;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle the event.
     *
     * @param  TwitterAccountWasLinked  $event
     * @return void
     */
    public function handle(TwitterAccountWasLinked $event)
    {
        Mail::to($event->user)->send(new TwitterAccountLinked($event->user));
    }
}
