<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Socials\GithubAccountWasLinked' => [
            'App\Listeners\Socials\SendGithubLinkedEmail',
        ],
        'App\Events\Socials\FacebookAccountWasLinked' => [
            'App\Listeners\Socials\SendFacebookLinkedEmail',
        ],
        'App\Events\Socials\TwitterAccountWasLinked' => [
            'App\Listeners\Socials\SendTwitterLinkedEmail',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
