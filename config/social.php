<?php

return [
	'services' => [

		'github' => [
			'name' => 'Github'
		],

		'twitter' => [
			'name' => 'Twitter'
		],

		'facebook' => [
			'name' => 'Facebook'
		],
	],

	'events' => [

		'github' => [
			'created' => \App\Events\Socials\GithubAccountWasLinked::class,
		],
		'facebook' => [
			'created' => \App\Events\Socials\FacebookAccountWasLinked::class,
		],
		'twitter' => [
			'created' => \App\Events\Socials\TwitterAccountWasLinked::class,
		],

	],
];